package dev.olog.nestedscrollview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Demonstrates the difference between a wrapped recycler view and one not wrapped
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // to see the difference, launch the layout inspector and see how many children
        // has both lists

        recycledList.adapter = Adapter()
        recycledList.layoutManager = LinearLayoutManager(this)

        notRecycledlist.adapter = Adapter()
        notRecycledlist.layoutManager = LinearLayoutManager(this)
    }
}

class Adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return object : RecyclerView.ViewHolder(view) {}
    }

    override fun getItemCount(): Int = 200

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

    }
}