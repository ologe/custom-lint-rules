## Custom lint detectors

### How to 
- create a java module
- edit build.gradle (lint module)
```groovy
apply plugin: 'java-library'

dependencies {
    compileOnly 'com.android.tools.lint:lint-api:26.5.3'
    compileOnly 'com.android.tools.lint:lint-checks:26.5.3'
}

jar {
    manifest {
        attributes 'Lint-Registry': '<lint-package-name>.CustomLintRegistry'
    }
}
```
- add lint module as a dependency, check build.gradle (app)
```groovy
dependencies {
    lintChecks project(':library')
}
```

### Resources
- [simple tutorial](https://jayrambhia.com/blog/android-lint)
- [android lint checks implementation](https://android.googlesource.com/platform/tools/base/+/master/lint/libs/lint-checks/src/main/java/com/android/tools/lint/checks)

