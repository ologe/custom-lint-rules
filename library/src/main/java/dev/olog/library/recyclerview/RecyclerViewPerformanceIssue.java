package dev.olog.library.recyclerview;

import com.android.tools.lint.detector.api.Category;
import com.android.tools.lint.detector.api.Implementation;
import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.Scope;
import com.android.tools.lint.detector.api.Severity;

@SuppressWarnings("UnstableApiUsage")
public class RecyclerViewPerformanceIssue {

    private static final String ID = "RecyclerViewPerformance";
    private static final String DESCRIPTION = "NestedScrollView can't be an ancestor of a RecyclerView";
    public static final String EXPLANATION = "NestedScrollView can't be an ancestor of a RecyclerView. \n" +
            "For some reason recycler view has to lay out all it's children when has a NestedScrollView ancestor.\n" + "https://gph.is/2cI3chF";
    private static final Category CATEGORY = Category.PERFORMANCE;
    private static final int PRIORITY = 10;

    // this won't break the build
    private static final Severity SEVERITY = Severity.ERROR;

    public static final Issue ISSUE = Issue.create(
            ID,
            DESCRIPTION,
            EXPLANATION,
            CATEGORY,
            PRIORITY,
            SEVERITY,
            new Implementation(
                    RecyclerViewPerformanceDetector.class,
                    Scope.RESOURCE_FILE_SCOPE
            )
    );

}
