package dev.olog.library.recyclerview;

import com.android.SdkConstants;
import com.android.annotations.Nullable;
import com.android.tools.lint.detector.api.LayoutDetector;
import com.android.tools.lint.detector.api.XmlContext;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.Arrays;
import java.util.Collection;

@SuppressWarnings("UnstableApiUsage")
public class RecyclerViewPerformanceDetector extends LayoutDetector {

    @Nullable
    @Override
    public Collection<String> getApplicableElements() {
        return Arrays.asList(
                SdkConstants.RECYCLER_VIEW.newName(),
                SdkConstants.RECYCLER_VIEW.oldName()
        );
    }

    @Override
    public void visitElement(@NotNull XmlContext context, @NotNull Element element) {
        Node parentNode = element.getParentNode();

        if (parentNode != null) {
            searchForNestedScrollViewAncestors(context, element, parentNode);
        }

    }

    private void searchForNestedScrollViewAncestors(
            @NotNull XmlContext context,
            @NotNull Element element,
            @NotNull Node parentNode
    ) {
        while (parentNode.getNodeType() == Node.ELEMENT_NODE) {
            // not root node

            Element current = (Element) parentNode;
            if (SdkConstants.NESTED_SCROLL_VIEW.isEquals(current.getTagName())) {
                // parent nested scroll view found, report and exit
                report(context, element);
                return;
            }

            parentNode = parentNode.getParentNode();
        }
    }

    private void report(
            @NotNull XmlContext context,
            @NotNull Element element
    ) {
        context.report(
                RecyclerViewPerformanceIssue.ISSUE,
                context.getLocation(element),
                RecyclerViewPerformanceIssue.EXPLANATION
        );
    }

}
