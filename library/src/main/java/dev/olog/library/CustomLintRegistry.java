package dev.olog.library;

import com.android.tools.lint.client.api.IssueRegistry;
import com.android.tools.lint.detector.api.Issue;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

import dev.olog.library.recyclerview.RecyclerViewPerformanceIssue;

@SuppressWarnings("UnstableApiUsage")
public class CustomLintRegistry extends IssueRegistry {

    @NotNull
    @Override
    public List<Issue> getIssues() {
        return Arrays.asList(
                RecyclerViewPerformanceIssue.ISSUE
        );
    }
}